message("-- SRE")

if(WIN32)

    execute_process(COMMAND ${PLATFORM_PATHS}/SRE/7z_portable/7za.exe x -y -o${PLATFORM_PATHS}/SRE/7z_portable ${PLATFORM_PATHS}/SRE/SRE.7z)
    file(RENAME ${PLATFORM_PATHS}/SRE/7z_portable/SRE.lib ${PLATFORM_PATHS}/SRE/SRE.lib)

    set(SRE_LIBRARY 
        ${PLATFORM_PATHS}/SRE/SRE.lib
        CACHE STRING "Where to find SRE library"
    )
elseif(UNIX)
    set(SRE_LIBRARY 
        ${PLATFORM_PATHS}/SRE/libSRE.a
        CACHE STRING "Where to find SRE library"
		)
endif()
