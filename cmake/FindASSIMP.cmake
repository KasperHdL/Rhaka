message("-- ASSIMP")

if(WIN32)
    set(ASSIMP_LIBRARY 
		${PLATFORM_PATHS}/assimp/${PLATFORM_ARCH}/assimp-vc140-mt.lib
        CACHE STRING "Where to find assimp-vc140-mt.lib"
		)
elseif(UNIX)
    set(ASSIMP_LIBRARY 
		${PLATFORM_PATHS}/assimp/libassimp.so
        CACHE STRING "Where to find libassimp.so"
		)
endif()
