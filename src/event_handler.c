#include <event_handler.h>
#include <cincl.h>

#include <stdio.h>
#include <stdlib.h>


EventFunction* functions;
int* subscribers;
int capacity_per_event;

#if DEBUG
char** function_names;
#endif

void eventhandler_initialize(int capacity){
    capacity_per_event = capacity;

    functions = calloc(EVENT_COUNT, sizeof(EventFunction) * capacity_per_event);
    subscribers = calloc(EVENT_COUNT, sizeof(int));

#if DEBUG
    function_names = calloc(EVENT_COUNT * capacity_per_event * 256, sizeof(char));
#endif

}

#if DEBUG
void _eventhandler_subscribe(const char* event_name, char* func_name, Event event, EventFunction function){
    if(subscribers[(int) event] > capacity_per_event){
        ERRORF("Cannot subscribe to Event(%i)", event);
        return;
    }

    printf("[Event:Subscribe:%s] %s\n", event_name, func_name);

    int num_subscribers = subscribers[(int)event]++;
    int index = (int) event + num_subscribers;
    functions[index] = function;
    function_names[index] = func_name;
    

}

void _eventhandler_unsubscribe(const char* event_name, const char* func_name, Event event, EventFunction function){
    if(subscribers[(int) event] == 0){
        ERRORF("Cannot unsubscribe from Event(%i) no one is subscribing", event);
        return;
    }

    printf("[Event:Subscribe:%s] %s\n", event_name, func_name);

    int num_subscribers = subscribers[(int)event]--;
    int index = (int) event + num_subscribers;
    functions[index] = NULL;
    function_names[index] = "";


}

void _eventhandler_trigger(const char* event_name, Event event, void* arguments){
    printf("[Event:Trigger:%s] calling: \n", event_name);

    for(int i = 0; i < subscribers[(int)event]; i++){
        int index = (int)event + i;
        printf("\t%s\n", function_names[index]);

        functions[index](arguments);
    }
}
#else
void _event_handler_trigger(Event event, void* arguments){
}
void _eventhandler_subscribe(Event event, EventFunction function){
}
void _eventhandler_unsubscribe(Event event, EventFunction function){
}
#endif
