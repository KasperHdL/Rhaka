#include <debug/logger_console.h>
#include <cincl.h>

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>

#include <debug/logger.h>


static char _log_buffer[2048];
static char _log_string[2048];

#ifdef __linux__
    const char* stop   = "\e[0m";
    const char* red    = "\e[1;31m";
    const char* green  = "\e[1;32m";
    const char* yellow = "\e[1;33m";
    const char* blue   = "\e[1;34m";
#else
	const char* stop   = "";
	const char* red    = "";
	const char* green  = "";
	const char* yellow = "";
	const char* blue   = "";
#endif

typedef enum{
    LOG_TYPE_Message,
    LOG_TYPE_Warning,
    LOG_TYPE_Error
} LogType;

static void _to_console(LogType log_type, char* file, const char* function, int line_number);

ILogger* get_console_logger(){
    ILogger* logger = malloc(sizeof(ILogger));
    *logger = (ILogger){
        .log        = &_log,
        .warning    = &_warning,
        .error      = &_error,
        .assert     = &_assert
    };

    return logger;
}


void _log(char* file, const char* function, int line_number, const char* string, ...){

    va_list argptr;
    va_start(argptr, string);
    vsprintf(_log_string, string, argptr); 
    va_end(argptr);

    _to_console(LOG_TYPE_Message, file, function, line_number);
}
void _warning(char* file, const char* function, int line_number, const char* string, ...){

    va_list argptr;
    va_start(argptr, string);
    vsprintf(_log_string, string, argptr); 
    va_end(argptr);

    _to_console(LOG_TYPE_Warning, file, function, line_number);
}
void _error(char* file, const char* function, int line_number, const char* string, ...){

    va_list argptr;
    va_start(argptr, string);
    vsprintf(_log_string, string, argptr); 
    va_end(argptr);

    _to_console(LOG_TYPE_Error, file, function, line_number);
}


void _assert(const char* condition, char* file, const char* function, int line_number, const char* string, ...){

    char* file_from_last_slash = strrchr(file, '/');
    if(file_from_last_slash == NULL){
        //if no slash found
        file_from_last_slash = file;
    }else{
        file_from_last_slash++;
    }

    sprintf(_log_buffer, "%sAssert%s(%s) [%s:%s:%i] ", red, stop, condition, file_from_last_slash, function, line_number);

    va_list argptr;
    va_start(argptr, string);

    vsprintf(_log_string, string, argptr); 
    strcat(_log_buffer, _log_string);

    fprintf(stderr, _log_buffer);
    fflush(stderr);
    va_end(argptr);


    exit(0);
}


void _to_console(LogType log_type, char* file, const char* function, int line_number){

    char* file_from_last_slash = strrchr(file, '/');
    if(file_from_last_slash == NULL){
        //if no slash found
        file_from_last_slash = file;
    }else{
        file_from_last_slash++;
    }

    switch(log_type){ //@Speed(Kasper) these could all be concated staticly in the macro
        case LOG_TYPE_Message:{
            sprintf(_log_buffer, "[%s:%s:%i] ", file_from_last_slash, function, line_number);
        }break;
        case LOG_TYPE_Warning:{
            sprintf(_log_buffer, "%sWarning%s [%s:%s:%i] ", yellow, stop, file_from_last_slash, function, line_number);
        }break;
        case LOG_TYPE_Error:{
            sprintf(_log_buffer, "%sERROR%s [%s:%s:%i] ", red, stop, file_from_last_slash, function, line_number);
        }break;
    }

    strcat(_log_buffer, _log_string);

    if(log_type == LOG_TYPE_Error){
        fprintf(stderr, _log_buffer);
        fflush(stderr);
    }else{
        fprintf(stdout, _log_buffer);
        fflush(stdout);
    }
}
