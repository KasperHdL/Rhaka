#include <context.h>

#include <debug/logger.h>
#include <debug/logger_console.h>

#include <memory/ring_allocator.h>
#include <memory/stack_allocator.h>

void context_set_defaults(){
    context.logger = get_console_logger();

    context.temp_allocator = get_ring_allocator();
    context.allocator = get_stack_allocator(10240);
}

void context_set_logger(u64 logger_hash){
    switch(logger_hash){
        case STATIC_HASH("console_logger", 3740961542):
            context.logger = get_console_logger();
        break;
        case STATIC_HASH("file_logger", 4113197746):
            context.logger = get_console_logger();
            WARNING("file logging is not implemented yet");
        break;
    }
}

IAllocator* get_allocator(u64 hash);

void context_set_allocator(u64 hash){
    context.allocator = get_allocator(hash);
}
void context_set_temp_allocator(u64 hash){
    context.temp_allocator = get_allocator(hash);
}

IAllocator* get_allocator(u64 hash){
    switch(hash){
        case STATIC_HASH("ring_allocator", 1288434153):
            return get_ring_allocator();
        break;
        case STATIC_HASH("stack_allocator", 1288434153):
            return get_stack_allocator();
        break;
 
    }
}
