#include <hincl.h>
#include <cincl.h>

#include <event_handler.h>
#include <stdio.h>

#include <memory/allocator.h>

u64 hello  = STATIC_HASH("BHa", 3002110632);
u64 hello1 = STATIC_HASH("BHa", 3002110632);
u64 hello2 = STATIC_HASH("BHA", 3069504534);
u64 hello3 = STATIC_HASH("BHA", 3069504534);
u64 hello4 = STATIC_HASH("BhA", 2102690457);
u64 hello5 = STATIC_HASH("BhA", 2102690457);

void callback(void* arguments){
    int* a = (int*) arguments;


    LOGF("callback %i", *a);

    LOGF("key %s, hash: %u", "BHa", STATIC_HASH("BHa", 3002110632));

    /*
     * Thinking about ECS DOD API
     */

    /*

    ///
    ITransform* system = (ITransform*) verse.get_system_and_select(SH("Transform"), entity);
    system->add_position(5,0,0);
    ///
    ITransform* system = (ITransform*) verse.get_system(SH("Transform"));
    system->select(entity);
    system->add_position(5,0,0);
    ///
    ITransform* system = (ITransform*) verse.get_system(SH("Transform"))->select(entity);
    system->add_position(5,0,0);
    ///
    ITransform* system = (ITransform*) verse.get_system(SH("Transform"))->select(entity);
    system->add_position(5,0,0);
    ///
    ITransform* system = (ITransform*) verse.get_system(SH("Transform"))->select(entity);
    vec3 pos = system->get_position();
    system->set_position(pos + (5,0,0);
    ///
    ((ITransform*) verse.get_system_and_select(SH("Transform"),entity))->add_position(5,0,0);
    ///
    //
    ITransform* system = (ITransform*) verse.get_system(SH("Transform")); //pointer could be stored

    TransformComponent t_comp = system->lookup(entity);
    system.data.positions[t_comp.id] += (5,0,0);//dirtying would have to work a different way then
    
    */



}

void event_test(){
    eventhandler_initialize(3);
    EVENT_SUBSCRIBE(EVENT_Test, &callback);

    int number = 1338;
    EVENT_TRIGGER(EVENT_Test, &number);

}

/*
void array_test(){
    Array_int arr = {};
    Array_float arrf;

    LOGF("(count: %i, capacity: %i, buffer: %p)", arr.count, arr.capacity, arr.buffer);
    LOGF("(count: %i, capacity: %i, buffer: %p)", arrf.count, arrf.capacity, arrf.buffer);

    array_allocate_int(&arr, 32);
    array_allocate_float(&arrf, 32);

    LOGF("(count: %i, capacity: %i, buffer: %p)", arr.count, arr.capacity, arr.buffer);
    LOGF("(count: %i, capacity: %i, buffer: %p)", arrf.count, arrf.capacity, arrf.buffer);

    array_set_int(&arr, 0, 1025);
    array_set_float(&arrf, 0, 1025);

    for(int i = 0; i < 30; i++){
        array_add_int(&arr, i);
        array_add_float(&arrf, i);
    }

    for(int i = 0; i < arr.count; i++){
        LOGF("%i: %i", i, array_get_int(&arr, i));
        LOGF("%i: %f", i, array_get_float(&arrf, i));
    }

    LOGF("(count: %i, capacity: %i, buffer: %p)", arr.count, arr.capacity, arr.buffer);
    LOGF("(count: %i, capacity: %i, buffer: %p)", arrf.count, arrf.capacity, arrf.buffer);
}
*/

int main() {
    context_set_defaults();
    event_test();
    return 0;
}
