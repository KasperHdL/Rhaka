# Good to know
## hincl.h
to be included in header files(.h). contains standard types(std_types.h) and context.h and the macro STATIC_HASH(string)

## cincl.h
to be included in implementation files (.c) contains logging macros (LOG, LOGF, WARNING, WARNINGF, ERROR, ERROF, ASSERT, ASSERTF).

## context.h
contains the context which contains common facilities for logging and allocation

## STATIC_HASH

# House Rules
- in header files only hincl.h is allowed to be included, anything else is to be forward declared.
