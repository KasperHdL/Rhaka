#pragma once
#include <hincl.h>

typedef enum {
    EVENT_Test,
    EVENT_SomeOtherTest,

    EVENT_COUNT
} Event;

struct EventArg_Test{
    int some_args;
    int other_args;
};
