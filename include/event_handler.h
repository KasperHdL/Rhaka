#pragma once
#include <hincl.h>

#include <events.h> //required because C cannot forward declare enums

typedef void (*EventFunction)(void* args);

void eventhandler_initialize(int capacity);


void eventhandler_enable_logging();
void eventhandler_disable_logging();


#if DEBUG
void _eventhandler_subscribe(const char* event_name, char* func_name, Event event, EventFunction function);
void _eventhandler_unsubscribe(const char* event_name, const char* func_name, Event event, EventFunction function);

void _eventhandler_trigger(const char* event_name, Event event, void* arguments);

#define EVENT_SUBSCRIBE(event, function) _eventhandler_subscribe(#event, #function, event, function)
#define EVENT_UNSUBSCRIBE(event, function) _eventhandler_unsubscribe(#event, #function, event, function)

#define EVENT_TRIGGER(event, args) _eventhandler_trigger(#event, event, args)

#else
void _eventhandler_subscribe(Event event, EventFunction function);
void _eventhandler_unsubscribe(Event event, EventFunction function);

void _eventhandler_trigger(Event event, void* arguments);

#define EVENT_SUBSCRIBE(event, function) _eventhandler_subscribe(event, function)
#define EVENT_UNSUBSCRIBE(event, function) _eventhandler_unsubscribe(event, function)

#define EVENT_TRIGGER(event, args) _eventhandler_trigger(event, args)

#endif
