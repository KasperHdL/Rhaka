#pragma once
#include <hincl.h>

/// FILO allocator
/// allocates a header after each allocation to ensure it can free

typedef struct IAllocator IAllocator;

void* _stack_allocate(IAllocator* allocator, u32 size);
void  _stack_deallocate(IAllocator* allocator, void* pointer);

u32 _stack_allocated_size(IAllocator* allocator, void* pointer);
u32 _stack_total_allocated(IAllocator* allocator);


IAllocator* get_stack_allocator();

