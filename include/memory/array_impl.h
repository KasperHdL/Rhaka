#ifdef TYPE

typedef struct {
    int capacity;
    int count;
    TYPE * buffer;
} GENERIC(Array);

void GENERIC(array_allocate) (GENERIC(Array)* array, int capacity){
    array->buffer = (TYPE *) calloc(capacity, sizeof(TYPE));
    array->capacity = capacity;
    array->count = 0;
}
void GENERIC(array_reallocate) (GENERIC(Array)* array, int capacity){
    ASSERTF(array->count > capacity, "Cannot allocate a capacity(%i) that is less than the current array count(%i)", capacity, array->count);

    array->buffer = (TYPE *) realloc(array->buffer, capacity * sizeof(TYPE));
    array->capacity = capacity;
}

void GENERIC(array_deallocate) (GENERIC(Array)* array){
    free(array->buffer);
}

void GENERIC(array_add) (GENERIC(Array)* array, TYPE value){
    if(array->count >= array->capacity){
        GENERIC(array_reallocate)(array, array->capacity * 2);
    }

    array->buffer[array->count++] = value;
}

void GENERIC(array_set) (GENERIC(Array)* array, int index, TYPE value){
    if(index >= array->capacity){
        GENERIC(array_reallocate)(array, array->capacity * 2);
    }

    if(index > array->count)
        array->count = index + 1;

    array->buffer[index] = value;
}

TYPE GENERIC(array_get) (GENERIC(Array)* array, int index){
    return array->buffer[index];
}
#endif
