#pragma once
#include <hincl.h>
#include <cincl.h>
#include <stdlib.h>

#define TYPE int
#define GENERIC(x) x##_int
#include <memory/array_impl.h>
#undef TYPE
#undef GENERIC

#define TYPE float
#define GENERIC(x) x##_float
#include <memory/array_impl.h>
#undef TYPE
#undef GENERIC

