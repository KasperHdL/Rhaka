#pragma once
#include <hincl.h>

//forward declarations
typedef struct ILogger ILogger;
typedef struct IAllocator IAllocator;

typedef struct Context{
    ILogger* logger;
    IAllocator* allocator;
    IAllocator* temp_allocator;
} Context;

Context context;

void context_set_defaults();

void context_set_logger(u64 logger_hash);
void context_set_allocator(u64 allocator_hash);
void context_set_temp_allocator(u64 allocator_hash);

