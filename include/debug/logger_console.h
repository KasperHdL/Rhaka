#pragma once
#include <hincl.h>

void _log     (char* file, const char* function, int line_number, const char* string, ...);
void _warning (char* file, const char* function, int line_number, const char* string, ...);
void _error   (char* file, const char* function, int line_number, const char* string, ...);

void _assert  (const char* condition, char* file, const char* function, int line_number, const char* string, ...);

typedef struct ILogger ILogger;
ILogger* get_console_logger();
