#pragma once
#include <hincl.h>

typedef struct ILogger{
    void (*log)     (char* file, const char* function, int line_number, const char* format, ...);
    void (*warning) (char* file, const char* function, int line_number, const char* format, ...);
    void (*error)   (char* file, const char* function, int line_number, const char* format, ...);
    void (*assert)  (const char* condition, char* file, const char* function, int line_number, const char* format, ...);
} ILogger;


#if DEBUG
#include <context.h>
#define LOG(x)       context.logger->log(__FILE__, __func__, __LINE__, x "\n")
#define LOGF(x, ...) context.logger->log(__FILE__, __func__, __LINE__, x "\n", __VA_ARGS__)

#define WARNING(x)       context.logger->warning(__FILE__, __func__, __LINE__, x "\n")
#define WARNINGF(x, ...) context.logger->warning(__FILE__, __func__, __LINE__, x "\n", __VA_ARGS__)

#define ERROR(x)       context.logger->error(__FILE__, __func__, __LINE__, x "\n")
#define ERRORF(x, ...) context.logger->error(__FILE__, __func__, __LINE__, x "\n", __VA_ARGS__)

#define ASSERT(b, x)       if(b){ (*context.logger->assert)(#b, __FILE__, __func__, __LINE__, x "\n");}
#define ASSERTF(b, x, ...) if(b){ (*context.logger->assert)(#b, __FILE__, __func__, __LINE__, x "\n", __VA_ARGS__);}

#else

#define LOG(x)       
#define LOGF(x, ...) 

#define WARNING(x)       
#define WARNINGF(x, ...) 

#define ERROR(x)       
#define ERRORF(x, ...) 

#define ASSERT(b, x)       
#define ASSERTF(b, x, ...) 

#endif
