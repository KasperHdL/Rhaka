#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "file_io.h"
#include "murmur_hash.h"

char* search_word = "STATIC_HASH(\"";
int search_word_length = 13;

typedef enum{
    Searching,
    WaitingForEndString,
    WritingHash,
    WaitingForParanthesis,
} State;

State state;

int process_file(char* file_name){

    long file_length;
    char* file = read_file_as_array(file_name, &file_length);
    if(file == NULL){
        printf("\t\t\tError reading file");
        return -1;
    }

    int key_index = 0;
    char* key = calloc(1, 128);
    uint64_t hash;

    int wfile_length = file_length * 2;
    char* wfile = NULL;

    int index = 0;
    int windex = 0;

    int word_index = 0;

    int temp;
    int num_found = 0;

    while(index < file_length){
        switch(state){
            case Searching:
                if(file[index] == search_word[word_index]){
                    word_index++;

                    if(word_index == search_word_length){
                        //Found word
                        state = WaitingForEndString;
                        num_found++;
                        word_index = 0;

                        //if write file is not allocated
                        if(wfile == NULL){
                            wfile = malloc(wfile_length);
                            if(wfile == NULL){
                                printf("\t\t\tError allocating write file");
                            }
                            for(int i = 0; i < index;i++){
                                wfile[windex++] = file[i];
                            }
                        }
                    }
                }else{
                    word_index = 0;
                }

                if(wfile != NULL)
                    wfile[windex++] = file[index];

                index++;
            break;
            case WaitingForEndString:
                if(file[index] == '\"'){
                    wfile[windex++] = file[index++];

                    //write comma aswell
                    if(file[index] == ','){
                        file[index++];
                    }
                    wfile[windex++] = ',';

                    state = WritingHash;
                    continue;
                }else{
                    key[key_index++] = file[index];
                }

                wfile[windex++] = file[index++];

            break;
            case WritingHash:
                key[key_index++] = '\0';
                hash = murmur_hash_64(key, key_index, 0);

                temp = snprintf(&wfile[windex], wfile_length - windex, " %u)", hash);
                windex += temp;
                key_index = 0;

                state = WaitingForParanthesis;

            break;
            case WaitingForParanthesis:
                if(file[index] == ')'){
                    state = Searching;
                }
                index++;
            break;
        }

    }

    if(num_found > 0){

        bool success = write_file_from_array(file_name, wfile, windex-1);

        if(!success){
            free(wfile);
            free(file);
            printf("\t\t\tError writing to file");
            return -1;
        }

    }


    free(wfile);
    free(file);
    return num_found;
}


int main(int argc, char **argv)
{
    printf("\n[StaticHasher]\n");

    for(int i = 1; i < argc; i++){
        int num_hashes = process_file(argv[i]);

        if(num_hashes > 0){
            printf("\t%i Hashes found [%s]\n", num_hashes, argv[i]); 
        }else if(num_hashes == -1){
            printf("\t\tError when processing: %s\n", argv[i]); 
        }
    }
    printf("\n");

    return 0;
}
